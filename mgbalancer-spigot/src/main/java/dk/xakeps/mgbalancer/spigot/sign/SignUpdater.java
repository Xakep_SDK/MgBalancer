package dk.xakeps.mgbalancer.spigot.sign;

import dk.xakeps.mgbalancer.shared.Balancer;
import dk.xakeps.mgbalancer.shared.Connectable;
import dk.xakeps.mgbalancer.shared.Server;
import dk.xakeps.mgbalancer.shared.ServerCollection;
import dk.xakeps.mgbalancer.shared.packet.data.SignTemplate;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.block.Sign;

import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.CompletableFuture;

public class SignUpdater implements Runnable {
    private final Balancer api;
    private final Map<Location, ConnectionSign> signs;

    private SignTemplateHolder holder;

    SignUpdater(Balancer api, Map<Location, ConnectionSign> signs) {
        this.api = api;
        this.signs = signs;
        this.holder = new SignTemplateHolder(api);
    }

    @Override
    public void run() {
        if(Bukkit.getOnlinePlayers().size() > 0) {
            api.getServers()
                    .thenCombine(holder.getTemplate(), ResultHolder::new)
                    .whenComplete((result, err) -> {
                if (err != null) {
                    return;
                }

                signs.forEach((location, connectionSign) -> {
                    Connectable connectable = connectionSign.getConnectable();
                    Sign sign = connectionSign.getSign();
                    if (connectable instanceof Server) {
                        Server oldSrv = (Server) connectable;
                        ServerCollection collection = result.getCollections().get(oldSrv.getCollectionName());
                        if(collection == null) {
                            updateSign(sign, false, oldSrv, result.getTemplate());
                        } else {
                            Optional<Server> serverOptional = collection.getServer(oldSrv.getName());
                            Server server = serverOptional.orElse(oldSrv);
                            connectionSign.setConnectable(server);
                            updateSign(sign, serverOptional.isPresent(), server, result.getTemplate());
                        }
                    } else if (connectable instanceof ServerCollection) {
                        ServerCollection collection = result.getCollections().get(connectable.getName());
                        if(collection == null) {
                            updateSign(sign, false, (ServerCollection) connectable, result.getTemplate());
                        } else {
                            updateSign(sign, true, collection, result.getTemplate());
                            connectionSign.setConnectable(collection);
                        }
                    }
                });
            });
        }
    }

    public void updateTemplate(SignTemplate template) {
        holder.setTemplate(template);
    }

    private static void updateSign(Sign sign, boolean online, ServerCollection collection, SignTemplate template) {
        List<String> lore = online ? template.getCollectionTemplate() : template.getOfflineCollectionTemplate();
        for (int i = 0; i < lore.size(); i++) {
            String loreLine = lore.get(i)
                    .replace("${online}", String.valueOf(collection.getOnline()))
                    .replace("${maxOnline}", String.valueOf(collection.getMaxOnline()))
                    .replace("${collName}", collection.getName())
                    .replace("${serverCount}", String.valueOf(collection.getServers().size()));

            sign.setLine(i, loreLine);
        }
        sign.update();
    }

    private static void updateSign(Sign sign, boolean online, Server server, SignTemplate template) {
        List<String> lore = online ? template.getServerTemplate() : template.getOfflineServerTemplate();
        for (int i = 0; i < lore.size(); i++) {
            String loreLine = lore.get(i)
                    .replace("${map}", server.getMap())
                    .replace("${online}", String.valueOf(server.getOnline()))
                    .replace("${maxOnline}", String.valueOf(server.getMaxOnline()))
                    .replace("${motd}", String.valueOf(server.getMotd()))
                    .replace("${name}", server.getName())
                    .replace("${collName}", server.getCollectionName());
            sign.setLine(i, loreLine);
        }
        sign.update();
    }

    private static final class ResultHolder {
        private final Map<String, ServerCollection> collections;
        private final SignTemplate template;

        private ResultHolder(Map<String, ServerCollection> collections, SignTemplate template) {
            this.collections = collections;
            this.template = template;
        }

        private Map<String, ServerCollection> getCollections() {
            return collections;
        }

        private SignTemplate getTemplate() {
            return template;
        }
    }

    private static final class SignTemplateHolder {
        private final Balancer api;
        private SignTemplate template;

        private SignTemplateHolder(Balancer api) {
            this.api = api;
        }

        private CompletableFuture<SignTemplate> getTemplate() {
            return template == null ? api.getTemplate()
                    .whenComplete((r,e) -> setTemplate(r)) : CompletableFuture.completedFuture(template);
        }

        private void setTemplate(SignTemplate template) {
            if(template == null) return;
            this.template = template;
        }
    }
}
