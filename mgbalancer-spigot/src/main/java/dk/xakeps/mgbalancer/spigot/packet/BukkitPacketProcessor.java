package dk.xakeps.mgbalancer.spigot.packet;

import dk.xakeps.mgbalancer.shared.ResponseQueue;
import dk.xakeps.mgbalancer.shared.packet.PacketProcessor;
import dk.xakeps.mgbalancer.shared.packet.PacketSender;
import dk.xakeps.mgbalancer.shared.packet.request.PacketSignTemplateUpdateRequest;
import dk.xakeps.mgbalancer.shared.packet.response.PacketConnectPlayerResponse;
import dk.xakeps.mgbalancer.shared.packet.response.PacketServerListResponse;
import dk.xakeps.mgbalancer.shared.packet.response.PacketSignTemplateResponse;
import dk.xakeps.mgbalancer.spigot.sign.SignManager;

import java.util.concurrent.CompletableFuture;

public class BukkitPacketProcessor implements PacketProcessor {
    private final ResponseQueue responseQueue;
    private final SignManager manager;

    public BukkitPacketProcessor(ResponseQueue responseQueue, SignManager manager) {
        this.responseQueue = responseQueue;
        this.manager = manager;
    }

    @Override
    public void process(PacketServerListResponse packet, PacketSender sender) {
        CompletableFuture<Object> response = responseQueue.getPacketResponse(packet.getRequestId());
        if(response != null) {
            response.complete(packet.getServers());
        }
    }

    @Override
    public void process(PacketConnectPlayerResponse packet, PacketSender sender) {
        CompletableFuture<Object> response = responseQueue.getPacketResponse(packet.getRequestId());
        if (response != null) {
            response.complete(packet.getResponse());
        }
    }

    @Override
    public void process(PacketSignTemplateResponse packet, PacketSender sender) {
        CompletableFuture<Object> response = responseQueue.getPacketResponse(packet.getRequestId());
        if (response != null) {
            response.complete(packet.getTemplate());
        }
    }

    @Override
    public void process(PacketSignTemplateUpdateRequest packet, PacketSender sender) {
        manager.updateTemplate(packet.getTemplate());
    }
}
