package dk.xakeps.mgbalancer.spigot.packet;

import dk.xakeps.mgbalancer.shared.Constants;
import dk.xakeps.mgbalancer.shared.packet.PacketSender;
import dk.xakeps.mgbalancer.spigot.Plugin;
import org.bukkit.Server;

public class BukkitPacketSender implements PacketSender {
    private final Plugin plugin;
    private final Server server;

    public BukkitPacketSender(Plugin plugin, Server server) {
        this.plugin = plugin;
        this.server = server;
    }

    @Override
    public void send(byte[] data) {
        server.sendPluginMessage(plugin, Constants.CHANNEL_NAME, data);
    }
}
