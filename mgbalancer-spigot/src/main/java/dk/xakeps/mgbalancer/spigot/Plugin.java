package dk.xakeps.mgbalancer.spigot;

import dk.xakeps.mgbalancer.shared.Balancer;
import dk.xakeps.mgbalancer.shared.Constants;
import dk.xakeps.mgbalancer.shared.ResponseQueue;
import dk.xakeps.mgbalancer.shared.packet.PacketSender;
import dk.xakeps.mgbalancer.spigot.gui.ServerListGui;
import dk.xakeps.mgbalancer.spigot.packet.BukkitPacketSender;
import dk.xakeps.mgbalancer.spigot.packet.MessageProcessor;
import dk.xakeps.mgbalancer.spigot.packet.BukkitPacketProcessor;
import dk.xakeps.mgbalancer.spigot.portal.PortalManager;
import dk.xakeps.mgbalancer.spigot.sign.SignManager;
import dk.xakeps.mgbalancer.spigot.storage.PortalStorage;
import dk.xakeps.mgbalancer.spigot.storage.SignStorage;
import org.bukkit.command.PluginCommand;
import org.bukkit.event.HandlerList;
import org.bukkit.plugin.ServicePriority;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.concurrent.TimeUnit;

public class Plugin extends JavaPlugin {
    @Override
    public void onEnable() {
        saveDefaultConfig();

        ResponseQueue queue = new ResponseQueue(TimeUnit.SECONDS.toMillis(5));
        PacketSender sender = new BukkitPacketSender(this, getServer());

        Balancer api = new SpigotBalancer(queue, sender);
        SignStorage storage = new SignStorage(getConfig(), this, api);
        SignManager signManager = new SignManager(this, api, storage);
        PortalManager portalManager = new PortalManager(new PortalStorage(getConfig(), this, api));
        getServer().getMessenger().registerIncomingPluginChannel(this, Constants.CHANNEL_NAME,
                new MessageProcessor(new BukkitPacketProcessor(queue, signManager), sender));
        getServer().getMessenger().registerOutgoingPluginChannel(this, Constants.CHANNEL_NAME);

        ServerListGui gui = new ServerListGui(api);
        getServer().getScheduler().runTaskTimerAsynchronously(this, gui, 0, 100);
        getServer().getPluginManager().registerEvents(gui, this);
        getServer().getPluginManager().registerEvents(signManager, this);
        getServer().getPluginManager().registerEvents(portalManager, this);

        getServer().getServicesManager().register(Balancer.class, api, this, ServicePriority.Highest);

        PluginCommand mgb = getServer().getPluginCommand("mgb");
        CommandProcessor commandProcessor = new CommandProcessor(this, api, signManager, portalManager, gui);
        mgb.setExecutor(commandProcessor);
        mgb.setTabCompleter(commandProcessor);

    }

    @Override
    public void onDisable() {
        getServer().getServicesManager().unregisterAll(this);
        getServer().getMessenger().unregisterIncomingPluginChannel(this);
        getServer().getMessenger().unregisterOutgoingPluginChannel(this);
        HandlerList.unregisterAll(this);
    }
}
