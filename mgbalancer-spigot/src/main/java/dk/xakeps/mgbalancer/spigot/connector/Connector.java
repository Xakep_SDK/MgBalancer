package dk.xakeps.mgbalancer.spigot.connector;

import dk.xakeps.mgbalancer.shared.Balancer;
import dk.xakeps.mgbalancer.shared.Connectable;
import dk.xakeps.mgbalancer.shared.packet.data.ConnectResponse;
import org.bukkit.entity.Player;

import java.util.HashSet;
import java.util.Set;
import java.util.UUID;

public class Connector {
    private final Balancer balancer;
    private Connectable connectable;
    private static final Set<UUID> connectingPlayers = new HashSet<>();

    public Connector(Balancer balancer, Connectable connectable) {
        this.balancer = balancer;
        this.connectable = connectable;
    }

    public Connectable getConnectable() {
        return connectable;
    }

    public void setConnectable(Connectable connectable) {
        this.connectable = connectable;
    }

    public void connect(Player player) {
        if (!connectingPlayers.contains(player.getUniqueId())) {
            connectingPlayers.add(player.getUniqueId());
            balancer.getPlayerFromId(player.getUniqueId())
                    .thenCompose(p -> balancer.connect(p, connectable))
                    .exceptionally(throwable -> new ConnectResponse(false, false))
                    .thenAccept(connectResponse -> {
                        connectingPlayers.remove(player.getUniqueId());
                        if (!connectResponse.isConnected()) {
                            player.sendMessage("[MGB] Сервер недоступен, попробуйте позже");
                        }
                    });
        }
    }
}
