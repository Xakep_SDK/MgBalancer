package dk.xakeps.mgbalancer.spigot.gui;

import dk.xakeps.mgbalancer.shared.Balancer;
import dk.xakeps.mgbalancer.shared.Server;
import dk.xakeps.mgbalancer.shared.ServerCollection;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.*;

public class ServerListGui implements Runnable, Listener {
    private final Balancer balancer;
    private final Inventory inventory;
    private final Map<String, CollectionInventory> inventories;

    public ServerListGui(Balancer balancer) {
        this.balancer = balancer;
        this.inventory = Bukkit.createInventory(null, 54, "[MGB] Collection list");
        this.inventories = new HashMap<>();
    }

    @Override
    public void run() {
        if(inventory.getViewers().size() > 0) {
            balancer.getServers()
                    .exceptionally(throwable -> null)
                    .thenAccept(coll -> {
                        if (coll == null)
                            return;

                        for (CollectionInventory ci : inventories.values()) {
                            ServerCollection collection = coll.get(ci.collection.getName());
                            ItemStack stack = getStack(collection == null ? ci.collection : collection, collection != null);
                            inventory.setItem(ci.position, stack);
                            if (collection != null) {
                                ci.updateCollection(collection);
                            }
                        }
                        int max = -1;
                        for (ServerCollection collection : coll.values()) {
                            if (!inventories.containsKey(collection.getName())) {
                                if (max == -1) {
                                    max = inventories.values().stream()
                                            .max(Comparator.comparingInt(o -> o.position))
                                            .map(i -> i.position)
                                            .orElse(-1) + 1;
                                }
                                inventory.setItem(max, getStack(collection, true));
                                inventories.put(collection.getName(), new CollectionInventory(max, collection));
                                max++;
                            }
                        }
                    });
        }
    }

    public Inventory getInventory() {
        return inventory;
    }

    private static final class CollectionInventory {
        private final Inventory inventory;
        private final int position;
        private ServerCollection collection;
        private Map<String, Integer> positions;

        public CollectionInventory(int position, ServerCollection collection) {
            this.position = position;
            this.collection = collection;
            this.inventory = Bukkit.createInventory(null, 54, "[MGB] Server list");
            this.positions = new HashMap<>();

            int i = 0;
            for (Server server : collection.getServers()) {
                inventory.setItem(i, getStack(server, true));
                positions.put(server.getName(), i);
                i++;
            }
        }

        public void updateCollection(ServerCollection newCollection) {
            for (Server oldSrv : collection.getServers()) {
                String name = oldSrv.getName();
                Optional<Server> update = newCollection.getServer(name);
                int pos = positions.get(name);
                inventory.setItem(pos, getStack(update.orElse(oldSrv), update.isPresent()));
            }
            int max = -1;
            for (Server update : newCollection.getServers()) {
                if (collection.getServer(update.getName()) == null) {
                    if(max == -1) {
                        max = positions.values().stream().max(Integer::compareTo).orElse(-1) + 1;
                    }
                    inventory.setItem(max, getStack(update, true));
                    positions.put(update.getName(), max);
                    max++;
                }
            }
            this.collection = newCollection;
        }

        private static ItemStack getStack(Server server, boolean online) {
            ItemStack stack = new ItemStack(Material.GRASS);
            ItemMeta itemMeta = stack.getItemMeta();
            itemMeta.setDisplayName((online ? ChatColor.GREEN : ChatColor.RED) + server.getName() );
            List<String> lore = new ArrayList<>(6);
            lore.add(ChatColor.WHITE + "Название: " + server.getName());
            lore.add(ChatColor.WHITE + "Коллекция: " + server.getCollectionName());
            lore.add(ChatColor.WHITE + "Карта: " + server.getMap());
            lore.add(ChatColor.WHITE + "Онлайн: " + server.getOnline() + " / " + server.getMaxOnline());
            lore.add(ChatColor.WHITE + "Motd: " + server.getMotd());
            if(online) {
                lore.add(ChatColor.GREEN + "ОНЛАЙН!");
            } else {
                lore.add(ChatColor.RED + "ОФФЛАЙН!");
            }
            itemMeta.setLore(lore);
            stack.setItemMeta(itemMeta);
            return stack;
        }
    }

    private static ItemStack getStack(ServerCollection collection, boolean online) {
        ItemStack stack = new ItemStack(Material.CHEST);
        ItemMeta itemMeta = stack.getItemMeta();
        itemMeta.setDisplayName((online ? ChatColor.GREEN : ChatColor.RED) + collection.getName());
        List<String> lore = new ArrayList<>(4);
        lore.add(ChatColor.WHITE + "Название: " + collection.getName());
        lore.add(ChatColor.WHITE + "Онлайн: " + collection.getOnline() + " / " + collection.getMaxOnline());
        lore.add(ChatColor.WHITE + "Кол-во серверов: " + collection.getServers().size());
        if(online) {
            lore.add(ChatColor.GREEN + "ОНЛАЙН!");
        } else {
            lore.add(ChatColor.RED + "ОФФЛАЙН!");
        }
        itemMeta.setLore(lore);
        stack.setItemMeta(itemMeta);
        return stack;
    }

    @EventHandler
    public void onInventoryClick(InventoryClickEvent event) {
        if(event.getClickedInventory() != null) {
            event.setCancelled(event.getClickedInventory().getName().equalsIgnoreCase("[MGB] Server list"));
        }

        if (inventory.equals(event.getClickedInventory())) {
            event.setCancelled(true);
            ItemStack currentItem = event.getCurrentItem();
            if(currentItem == null || !currentItem.hasItemMeta()) {
                return;
            }
            String s = ChatColor.stripColor(currentItem.getItemMeta().getDisplayName());
            CollectionInventory collectionInventory = inventories.get(s);
            if(collectionInventory != null) {
                event.getWhoClicked().openInventory(collectionInventory.inventory);
            }
        }
    }
}
