package dk.xakeps.mgbalancer.spigot.storage;

import dk.xakeps.mgbalancer.shared.Balancer;
import dk.xakeps.mgbalancer.spigot.Plugin;
import org.bukkit.configuration.Configuration;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public abstract class Storage<K,V> {
    private final String entryName;
    private final Configuration configuration;
    private final Plugin plugin;
    private final Map<K, V> content;

    Storage(String entryName, Configuration configuration, Plugin plugin, Balancer api) {
        this.entryName = entryName;
        this.configuration = configuration;
        this.plugin = plugin;
        this.content = deserialize(api, configuration.getMapList(entryName));
    }

    public Map<K, V> getContent() {
        return content;
    }

    public void save() {
        List<Map<String, Object>> serialized = new ArrayList<>(content.size());
        for (V value : content.values()) {
            serialized.add(serialize(value));
        }
        configuration.set(entryName, serialized);
        plugin.saveConfig();
    }

    protected abstract Map<String, Object> serialize(V value);

    protected abstract Map<K,V> deserialize(Balancer api, List<Map<?,?>> mapList);
}
