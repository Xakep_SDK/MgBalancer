package dk.xakeps.mgbalancer.spigot.sign;

import dk.xakeps.mgbalancer.shared.Balancer;
import dk.xakeps.mgbalancer.shared.Connectable;
import dk.xakeps.mgbalancer.spigot.connector.Connector;
import org.bukkit.Location;
import org.bukkit.block.Sign;

public class ConnectionSign extends Connector {
    private final Location location;

    public ConnectionSign(Balancer balancer, Connectable connectable, Location location) {
        super(balancer, connectable);
        this.location = location;
    }

    public Location getLocation() {
        return location;
    }

    public Sign getSign() {
        return (Sign) location.getBlock().getState();
    }
}
