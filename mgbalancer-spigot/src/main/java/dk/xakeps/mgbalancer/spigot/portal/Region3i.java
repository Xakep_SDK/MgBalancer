package dk.xakeps.mgbalancer.spigot.portal;

import org.bukkit.Location;
import org.bukkit.World;

public class Region3i {
    private final World world;
    private final Rectangle x;
    private final Rectangle y;
    private final Rectangle z;

    public Region3i(World world, Rectangle x, Rectangle y, Rectangle z) {
        this.world = world;
        this.x = x;
        this.y = y;
        this.z = z;
    }

    public World getWorld() {
        return world;
    }

    public Rectangle getX() {
        return x;
    }

    public Rectangle getY() {
        return y;
    }

    public Rectangle getZ() {
        return z;
    }

    public boolean contains(Location loc) {
        return loc.getWorld().equals(world)
                && x.isBetween(loc.getBlockX())
                && y.isBetween(loc.getBlockY())
                && z.isBetween(loc.getBlockZ());
    }

    public static final class Rectangle {
        private final int min;
        private final int max;

        private Rectangle(int min, int max) {
            this.min = Math.min(min, max);
            this.max = Math.max(min, max);
        }

        public int getMin() {
            return min;
        }

        public int getMax() {
            return max;
        }

        public boolean isBetween(int i) {
            return i >= getMin() && i <= getMax();
        }

        @Override
        public String toString() {
            return "Rectangle{" +
                    "min=" + min +
                    ", max=" + max +
                    '}';
        }

        public static Rectangle of(int a, int b) {
            return new Rectangle(a, b);
        }
    }

    @Override
    public String toString() {
        return "Region3i{" +
                "world=" + world +
                ", x=" + x +
                ", y=" + y +
                ", z=" + z +
                '}';
    }
}
