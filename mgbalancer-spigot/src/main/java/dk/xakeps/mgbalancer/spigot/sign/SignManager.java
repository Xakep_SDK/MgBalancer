package dk.xakeps.mgbalancer.spigot.sign;

import dk.xakeps.mgbalancer.shared.Balancer;
import dk.xakeps.mgbalancer.shared.Server;
import dk.xakeps.mgbalancer.shared.ServerCollection;
import dk.xakeps.mgbalancer.shared.packet.data.SignTemplate;
import dk.xakeps.mgbalancer.spigot.Plugin;
import dk.xakeps.mgbalancer.spigot.storage.SignStorage;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.block.Block;
import org.bukkit.block.Sign;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.SignChangeEvent;
import org.bukkit.event.player.PlayerInteractEvent;

import java.util.Map;
import java.util.Optional;

public class SignManager implements Listener {
    private final Balancer balancer;
    private final Map<Location, ConnectionSign> signs;
    private final SignUpdater updater;
    private final SignStorage storage;

    public SignManager(Plugin plugin, Balancer balancer, SignStorage storage) {
        this.balancer = balancer;
        this.signs = storage.getContent();
        this.updater = new SignUpdater(balancer, signs);
        this.storage = storage;
        Bukkit.getScheduler().runTaskTimerAsynchronously(plugin, updater, 0, 20);
    }

    public void updateTemplate(SignTemplate template) {
        updater.updateTemplate(template);
    }

    @EventHandler
    public void onSignCreate(SignChangeEvent event) {
        String identifier = event.getLine(0);
        if (!identifier.equalsIgnoreCase("[mgb]")) {
            return;
        }

        Player player = event.getPlayer();
        if (!player.hasPermission("mgb.use")) {
            return;
        }

        String signType = event.getLine(1);
        if (signType.equalsIgnoreCase("server")) {
            player.sendMessage("[MGB] Получаю список серверов...");
            balancer.getServers()
                    .exceptionally(throwable -> {
                        player.sendMessage("[MGB] Произошла ошибка при получении списка серверов!");
                        return null;
                    }).thenAccept(collections -> {
                        if(collections == null) return;
                        String serverName = event.getLine(2);
                        String collectionName;
                        try {
                            collectionName = Server.getCollectionName(serverName);
                        } catch (IllegalArgumentException e) {
                            player.sendMessage("[MGB] Неверное название сервера!");
                            return;
                        }

                        ServerCollection collection = collections.get(collectionName);
                        if (collection == null) {
                            player.sendMessage(String.format("[MGB] Коллекция под именем %s не найдена!", collectionName));
                            return;
                        }

                        Optional<Server> server = collection.getServer(serverName);
                        if (server.isPresent()) {
                            Server srv = server.get();
                            Location location = event.getBlock().getLocation();
                            signs.put(location, new ConnectionSign(balancer, srv, location));
                            storage.save();
                            player.sendMessage("[MGB] Табличка создана!");
                        } else {
                            player.sendMessage(String.format("[MGB] Сервер под именем %s не найден!", serverName));
                        }
                    });
        } else if (signType.equalsIgnoreCase("coll")) {
            player.sendMessage("[MGB] Получаю список серверов...");
            balancer.getServers()
                    .exceptionally(throwable -> {
                        player.sendMessage("[MGB] Произошла ошибка при получении списка серверов!");
                        return null;
                    })
                    .thenAccept(collections -> {
                        if (collections == null) return;
                        String collectionName = event.getLine(2);
                        ServerCollection collection = collections.get(collectionName);
                        if (collection == null) {
                            player.sendMessage(String.format("[MGB] Коллекция под именем %s не найдена!", collectionName));
                        } else {
                            Location location = event.getBlock().getLocation();
                            signs.put(location, new ConnectionSign(balancer, collection, location));
                            storage.save();
                            player.sendMessage("[MGB] Табличка создана!");
                        }
                    });
        }
    }

    @EventHandler
    public void onInteract(PlayerInteractEvent event) {
        if (!event.hasBlock()) {
            return;
        }
        ConnectionSign connectionSign = signs.get(event.getClickedBlock().getLocation());
        if (connectionSign == null) {
            return;
        }

        connectionSign.connect(event.getPlayer());
    }

    @EventHandler
    public void onBlockBreak(BlockBreakEvent event) {
        Block block = event.getBlock();
        if (block.getState() instanceof Sign) {
            signs.remove(block.getLocation());
        }
    }
}
