package dk.xakeps.mgbalancer.spigot.packet;

import dk.xakeps.mgbalancer.shared.packet.Packet;
import dk.xakeps.mgbalancer.shared.packet.PacketProcessor;
import dk.xakeps.mgbalancer.shared.packet.PacketSender;
import org.bukkit.entity.Player;
import org.bukkit.plugin.messaging.PluginMessageListener;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.OptionalDataException;

public class MessageProcessor implements PluginMessageListener {
    private final PacketProcessor processor;
    private final PacketSender sender;

    public MessageProcessor(PacketProcessor processor, PacketSender sender) {
        this.processor = processor;
        this.sender = sender;
    }

    @Override
    public void onPluginMessageReceived(String tag, Player player, byte[] data) {
        try (ObjectInputStream ois = new ObjectInputStream(new ByteArrayInputStream(data))) {
            Packet packet = (Packet) ois.readObject();
            packet.process(processor, sender);
        } catch (OptionalDataException ignored) {
        } catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

}
