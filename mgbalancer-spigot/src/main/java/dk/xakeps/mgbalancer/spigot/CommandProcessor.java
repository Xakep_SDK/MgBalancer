package dk.xakeps.mgbalancer.spigot;

import dk.xakeps.mgbalancer.shared.Balancer;
import dk.xakeps.mgbalancer.shared.Connectable;
import dk.xakeps.mgbalancer.shared.Server;
import dk.xakeps.mgbalancer.shared.ServerCollection;
import dk.xakeps.mgbalancer.spigot.gui.ServerListGui;
import dk.xakeps.mgbalancer.spigot.portal.ConnectionPortal;
import dk.xakeps.mgbalancer.spigot.portal.PortalManager;
import dk.xakeps.mgbalancer.spigot.portal.Region3i;
import dk.xakeps.mgbalancer.spigot.sign.SignManager;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.command.TabCompleter;
import org.bukkit.conversations.*;
import org.bukkit.entity.Player;

import java.util.*;

public class CommandProcessor implements CommandExecutor, ConversationAbandonedListener, TabCompleter {
    private final Balancer balancer;
    private final SignManager signManager;
    private final PortalManager portalManager;
    private final ConversationFactory conversationFactory;
    private final ServerListGui gui;

    CommandProcessor(Plugin plugin, Balancer balancer, SignManager signManager, PortalManager portalManager, ServerListGui gui) {
        this.balancer = balancer;
        this.signManager = signManager;
        this.portalManager = portalManager;
        this.conversationFactory = new ConversationFactory(plugin)
                .withModality(true)
                .withPrefix(new PortalConversationPrefix())
                .withFirstPrompt(new PortalNamePrompt())
                .withEscapeSequence("/exit")
                .withTimeout(15)
                .thatExcludesNonPlayersWithMessage("This is in-game feature!")
                .addConversationAbandonedListener(this);
        this.gui = gui;
    }

    @Override
    public boolean onCommand(CommandSender commandSender, Command command, String label, String[] args) {
        if(args.length == 0) {
            return false;
        }

        if(args[0].equalsIgnoreCase("reload")) {
            balancer.getTemplate().exceptionally(throwable -> {
                commandSender.sendMessage("[MGB] Ошибка обновления шаблона!");
                return null;
            }).thenAccept(signTemplate -> {
                if(signTemplate == null) return;
                commandSender.sendMessage("[MGB] Шаблон обновлён!");
                signManager.updateTemplate(signTemplate);
            });
            return true;
        }

        if(args[0].equalsIgnoreCase("create")) {
            if(commandSender instanceof Player) {
                conversationFactory.buildConversation((Conversable) commandSender).begin();
            } else {
                commandSender.sendMessage("[MGB] Доступно только из игры!");
            }
            return true;
        }

        if(args[0].equalsIgnoreCase("gui")) {
            if(commandSender instanceof Player) {
                ((Player) commandSender).openInventory(gui.getInventory());
            } else {
                commandSender.sendMessage("[MGB] Доступно только из игры!");
            }
            return true;
        }

        if(args.length == 2 && args[0].equalsIgnoreCase("remove")) {
            ConnectionPortal portal = portalManager.removePortal(args[1]);
            if (portal != null) {
                setMaterial(portal.getRegion(), Material.AIR);
                commandSender.sendMessage("[MGB] Портал бы удалён!");
            } else {
                commandSender.sendMessage("[MGB] Портал не найден!");
            }
            return true;
        }


        return false;
    }

    @Override
    public void conversationAbandoned(ConversationAbandonedEvent event) {
        ConversationContext context = event.getContext();
        if (event.gracefulExit()) {
            String name = (String) context.getSessionData("name");
            String type = (String) context.getSessionData("type");
            String serverId = (String) context.getSessionData("serverId");
            Location pos1 = (Location) context.getSessionData("pos1");
            Location pos2 = (Location) context.getSessionData("pos2");
            Material material = (Material) context.getSessionData("material");

            Region3i.Rectangle x = Region3i.Rectangle.of(pos1.getBlockX(), pos2.getBlockX());
            Region3i.Rectangle y = Region3i.Rectangle.of(pos1.getBlockY(), pos2.getBlockY());
            Region3i.Rectangle z = Region3i.Rectangle.of(pos1.getBlockZ(), pos2.getBlockZ());

            Region3i region3i = new Region3i(pos1.getWorld(), x, y, z);

            context.getForWhom().sendRawMessage("[MGB] Создаю портал...");

            balancer.getServers()
                    .exceptionally(throwable -> {
                        context.getForWhom().sendRawMessage("[MGB] Не удалось получить список серверов!");
                        return null;
                    })
                    .whenComplete((coll, throwable) -> {
                if(coll == null) return;

                switch (type) {
                    case "server":
                        try {
                            ServerCollection collection = coll.get(Server.getCollectionName(serverId));
                            Optional<Server> server = collection.getServer(serverId);
                            if (server.isPresent()) {
                                createPortal(name, server.get(), region3i, material);
                                context.getForWhom().sendRawMessage("[MGB] Портал создан!");
                            } else {
                                context.getForWhom().sendRawMessage("[MGB] Ошибка: сервер не найден");
                            }
                        } catch (IllegalArgumentException e) {
                            context.getForWhom().sendRawMessage("[MGB] Неверное название сервера!");
                            return;
                        }
                        break;

                    case "collection":
                        ServerCollection collection = coll.get(serverId);
                        if(collection == null) {
                            context.getForWhom().sendRawMessage("[MGB] Ошибка: коллекция не найдена!");
                        } else {
                            createPortal(name, collection, region3i, material);
                            context.getForWhom().sendRawMessage("[MGB] Портал создан!");
                        }
                        break;
                    default:
                        context.getForWhom().sendRawMessage("[MGB] Ошибка: неизвестный тип=" + type);
                        break;
                }
            });
        } else {
            context.getForWhom().sendRawMessage("[MGB] Создание портало отменено!");
        }
    }

    private void createPortal(String name, Connectable connectable, Region3i region3i, Material material) {
        ConnectionPortal portal = new ConnectionPortal(balancer, connectable, name, region3i);
        portalManager.addPortal(portal);
        setMaterial(region3i, material);
    }

    private void setMaterial(Region3i region3i, Material material) {
        World world = region3i.getWorld();
        Region3i.Rectangle x = region3i.getX();
        Region3i.Rectangle y = region3i.getY();
        Region3i.Rectangle z = region3i.getZ();

        for (int xPos = x.getMin(); xPos <= x.getMax(); xPos++) {
            for (int yPos = y.getMin(); yPos <= y.getMax(); yPos++) {
                for (int zPos = z.getMin(); zPos <= z.getMax(); zPos++) {
                    world.getBlockAt(xPos, yPos, zPos).setType(material);
                }
            }
        }
    }

    @Override
    public List<String> onTabComplete(CommandSender commandSender, Command command, String s, String[] args) {
        if(args.length > 0) {
            if(args[0].equalsIgnoreCase("remove")) {
                return new ArrayList<>(portalManager.getPortalNames());
            } else {
                return null;
            }
        } else {
            return Arrays.asList("reload", "create", "remove", "gui");
        }
    }

    private final class PortalNamePrompt extends ValidatingPrompt {

        @Override
        public String getPromptText(ConversationContext conversationContext) {
            return "Напишите имя портала";
        }

        @Override
        protected boolean isInputValid(ConversationContext conversationContext, String s) {
            if (portalManager.getPortalNames().contains(s)) {
                conversationContext.getForWhom().sendRawMessage("Портал с таким именем уже существует!");
                return false;
            } else {
                return true;
            }
        }

        @Override
        protected Prompt acceptValidatedInput(ConversationContext conversationContext, String s) {
            conversationContext.setSessionData("name", s);
            return new PortalTypePrompt();
        }
    }

    private final class PortalTypePrompt extends FixedSetPrompt {

        PortalTypePrompt() {
            super("server", "collection");
        }

        @Override
        protected Prompt acceptValidatedInput(ConversationContext conversationContext, String s) {
            conversationContext.setSessionData("type", s);
            switch (s) {
                case "server":
                    return new PortalServerNamePrompt();
                case "collection":
                    return new PortalCollectionNamePrompt();
                default:
                    return new PortalTypePrompt();
            }
        }

        @Override
        public String getPromptText(ConversationContext conversationContext) {
            return "Выберите тип портала. Типы: " + formatFixedSet();
        }
    }

    private final class PortalServerNamePrompt extends StringPrompt {
        @Override
        public String getPromptText(ConversationContext conversationContext) {
            return "Введите название сервера";
        }

        @Override
        public Prompt acceptInput(ConversationContext conversationContext, String s) {
            conversationContext.setSessionData("serverId", s);
            return new PortalPosPrompt();
        }
    }

    private final class PortalCollectionNamePrompt extends StringPrompt {

        @Override
        public String getPromptText(ConversationContext conversationContext) {
            return "Введите название коллекции";
        }

        @Override
        public Prompt acceptInput(ConversationContext conversationContext, String s) {
            conversationContext.setSessionData("serverId", s);
            return new PortalPosPrompt();
        }
    }

    private final class PortalPosPrompt extends FixedSetPrompt {
        PortalPosPrompt() {
            super("pos1", "pos2");
        }

        @Override
        protected Prompt acceptValidatedInput(ConversationContext conversationContext, String s) {
            Conversable forWhom = conversationContext.getForWhom();
            if (forWhom instanceof Player) {
                ((Player) forWhom).sendMessage(String.format("[MGB] Позиция %s установлена!", s));
                conversationContext.setSessionData(s, ((Player) forWhom).getLocation());
                Map<Object, Object> sessionData = conversationContext.getAllSessionData();
                if (sessionData.containsKey("pos1")
                        && sessionData.containsKey("pos2")) {
                    return new PortalMaterialPrompt();
                } else {
                    return new PortalPosPrompt();
                }
            } else {
                return Prompt.END_OF_CONVERSATION;
            }
        }

        @Override
        public String getPromptText(ConversationContext conversationContext) {
            return "Установите позиции портала с помощью команд: " + formatFixedSet();
        }
    }

    private final class PortalMaterialPrompt extends ValidatingPrompt {
        @Override
        public String getPromptText(ConversationContext conversationContext) {
            return "Напишите название блока для портала";
        }

        @Override
        protected boolean isInputValid(ConversationContext conversationContext, String s) {
            Material material = Material.matchMaterial(s);
            return material != null && material.isBlock();
        }

        @Override
        protected Prompt acceptValidatedInput(ConversationContext conversationContext, String s) {
            conversationContext.setSessionData("material", Material.matchMaterial(s));
            return Prompt.END_OF_CONVERSATION;
        }
    }

    private final class PortalConversationPrefix implements ConversationPrefix {

        @Override
        public String getPrefix(ConversationContext conversationContext) {
            String name = (String) conversationContext.getSessionData("name");
            String type = (String) conversationContext.getSessionData("type");
            String serverId = (String) conversationContext.getSessionData("serverId");
            Location pos1 = (Location) conversationContext.getSessionData("pos1");
            Location pos2 = (Location) conversationContext.getSessionData("pos2");
            Material material = (Material) conversationContext.getSessionData("material");
            // AUTOGENERATED
            return "================" + '\n' +
                    ChatColor.GOLD + "Название: " + ChatColor.WHITE + (name == null ? ChatColor.RED : ChatColor.GREEN) + (name == null ? "Не установлено" : name) + '\n' +
                    ChatColor.GOLD + "Тип: " + ChatColor.WHITE + (type == null ? ChatColor.RED : ChatColor.GREEN) + (type == null ? "Не установлен" : type) + '\n' +
                    ChatColor.GOLD + "ID: " + ChatColor.WHITE + (serverId == null ? ChatColor.RED : ChatColor.GREEN) + (serverId == null ? "Не установлен" : serverId) + '\n' +
                    ChatColor.GOLD + "Позиция 1: " + ChatColor.WHITE + (pos1 == null ? ChatColor.RED : ChatColor.GREEN) + (pos1 == null ? "Не установлена" : "Установлена") + '\n' +
                    ChatColor.GOLD + "Позиция 2: " + ChatColor.WHITE + (pos2 == null ? ChatColor.RED : ChatColor.GREEN) + (pos2 == null ? "Не установлена" : "Установлена") + '\n' +
                    ChatColor.GOLD + "Материал: " + ChatColor.WHITE + (material == null ? ChatColor.RED : ChatColor.GREEN) + (material == null ? "Не установлен" : material.toString()) + '\n' +
                    ChatColor.RESET + "================" + '\n';
        }
    }
}
