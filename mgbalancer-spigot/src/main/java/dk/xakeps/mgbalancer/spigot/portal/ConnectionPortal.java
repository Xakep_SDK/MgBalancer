package dk.xakeps.mgbalancer.spigot.portal;

import dk.xakeps.mgbalancer.shared.Balancer;
import dk.xakeps.mgbalancer.shared.Connectable;
import dk.xakeps.mgbalancer.spigot.connector.Connector;

public class ConnectionPortal extends Connector {
    private final String name;
    private final Region3i region;

    public ConnectionPortal(Balancer balancer, Connectable connectable, String name, Region3i region) {
        super(balancer, connectable);
        this.name = name;
        this.region = region;
    }

    public String getName() {
        return name;
    }

    public Region3i getRegion() {
        return region;
    }
}
