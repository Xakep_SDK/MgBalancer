package dk.xakeps.mgbalancer.spigot.storage;

import dk.xakeps.mgbalancer.shared.Balancer;
import dk.xakeps.mgbalancer.shared.Server;
import dk.xakeps.mgbalancer.shared.ServerCollection;
import dk.xakeps.mgbalancer.spigot.Plugin;
import dk.xakeps.mgbalancer.spigot.sign.ConnectionSign;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.block.Sign;
import org.bukkit.configuration.Configuration;

import java.util.*;

public class SignStorage extends Storage<Location, ConnectionSign> {
    public SignStorage(Configuration configuration, Plugin plugin, Balancer api) {
        super("signs", configuration, plugin, api);
        save(); // may remove broken entries
    }

    @Override
    protected Map<Location, ConnectionSign> deserialize(Balancer api, List<Map<?, ?>> signs) {
        Map<Location, ConnectionSign> ret = new HashMap<>();
        for (Map<?, ?> sign : signs) {
            String world = (String) sign.get("world");
            int x = (Integer) sign.get("x");
            int y = (Integer) sign.get("y");
            int z = (Integer) sign.get("z");
            String type = (String) sign.get("type");
            String name = (String) sign.get("name");

            World worldObj = Bukkit.getWorld(world);
            Location loc = new Location(worldObj, x, y, z);
            if(!(loc.getBlock().getState() instanceof Sign)) {
                continue;
            }
            if (type.equalsIgnoreCase("server")) {
                ret.put(loc, new ConnectionSign(api, new Server(name, "", "", 0, 0), loc));
            } else if (type.equalsIgnoreCase("collection")) {
                ret.put(loc, new ConnectionSign(api, new ServerCollection(name, Collections.emptyMap()), loc));
            }
        }
        return ret;
    }

    @Override
    protected Map<String, Object> serialize(ConnectionSign sign) {
        Map<String, Object> ret = new LinkedHashMap<>();
        Location loc = sign.getLocation();
        ret.put("world", loc.getWorld().getName());
        ret.put("x", loc.getBlockX());
        ret.put("y", loc.getBlockY());
        ret.put("z", loc.getBlockZ());
        ret.put("type", sign.getConnectable() instanceof Server ? "server" : "collection");
        ret.put("name", sign.getConnectable().getName());
        return ret;
    }
}
