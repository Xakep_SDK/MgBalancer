package dk.xakeps.mgbalancer.spigot;

import dk.xakeps.mgbalancer.shared.*;
import dk.xakeps.mgbalancer.shared.packet.Packet;
import dk.xakeps.mgbalancer.shared.packet.PacketSender;
import dk.xakeps.mgbalancer.shared.packet.data.ConnectResponse;
import dk.xakeps.mgbalancer.shared.packet.data.SignTemplate;
import dk.xakeps.mgbalancer.shared.packet.request.PacketConnectPlayerToCollectionRequest;
import dk.xakeps.mgbalancer.shared.packet.request.PacketConnectPlayerToServerRequest;
import dk.xakeps.mgbalancer.shared.packet.request.PacketServerListRequest;
import dk.xakeps.mgbalancer.shared.packet.request.PacketSignTemplateRequest;
import org.bukkit.Bukkit;

import java.io.IOException;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.CompletableFuture;

public class SpigotBalancer implements Balancer {
    private final ResponseQueue responseQueue;
    private final PacketSender sender;

    SpigotBalancer(ResponseQueue responseQueue, PacketSender sender) {
        this.responseQueue = responseQueue;
        this.sender = sender;
    }

    @Override
    public CompletableFuture<ConnectResponse> connect(BalancerPlayer player, Server server) {
        Packet request = new PacketConnectPlayerToServerRequest(player.getUniqueId(), server.getName());
        return registerAndSendPacket(request);
    }

    @Override
    public CompletableFuture<ConnectResponse> connect(BalancerPlayer player, ServerCollection collection) {
        Packet request = new PacketConnectPlayerToCollectionRequest(player.getUniqueId(), collection.getName());
        return registerAndSendPacket(request);
    }

    @Override
    public CompletableFuture<SignTemplate> getTemplate() {
        Packet request = new PacketSignTemplateRequest();
        return registerAndSendPacket(request);
    }

    @Override
    public CompletableFuture<Map<String, ServerCollection>> getServers() {
        Packet request = new PacketServerListRequest();
        return registerAndSendPacket(request);
    }

    @Override
    public PacketSender getSender() {
        return sender;
    }

    @Override
    public PacketSender getSender(Server server) {
        throw new UnsupportedOperationException();
    }

    @Override
    public CompletableFuture<BalancerPlayer> getPlayerFromId(UUID playerID) {
        return CompletableFuture.completedFuture(() -> Bukkit.getPlayer(playerID).getUniqueId());
    }

    private <T> CompletableFuture<T> registerAndSendPacket(Packet packet) {
        CompletableFuture<T> future = new CompletableFuture<>();
        responseQueue.registerPacketResponse(packet, future);
        sendPacket(packet);
        return future;
    }

    private void sendPacket(Packet packet) {
        try {
            getSender().send(packet);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
