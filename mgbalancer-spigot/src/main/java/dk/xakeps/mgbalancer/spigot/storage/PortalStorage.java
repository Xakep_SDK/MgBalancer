package dk.xakeps.mgbalancer.spigot.storage;

import dk.xakeps.mgbalancer.shared.Balancer;
import dk.xakeps.mgbalancer.shared.Server;
import dk.xakeps.mgbalancer.shared.ServerCollection;
import dk.xakeps.mgbalancer.spigot.Plugin;
import dk.xakeps.mgbalancer.spigot.portal.ConnectionPortal;
import dk.xakeps.mgbalancer.spigot.portal.Region3i;
import org.bukkit.Bukkit;
import org.bukkit.World;
import org.bukkit.configuration.Configuration;

import java.util.*;

public final class PortalStorage extends Storage<String, ConnectionPortal> {

    public PortalStorage(Configuration configuration, Plugin plugin, Balancer api) {
        super("portals", configuration, plugin, api);
    }

    @Override
    protected Map<String, ConnectionPortal> deserialize(Balancer api, List<Map<?, ?>> portals) {
        Map<String, ConnectionPortal> ret = new HashMap<>();
        for (Map<?, ?> portal : portals) {
            String world = (String) portal.get("world");
            int xMin = (Integer) portal.get("xMin");
            int yMin = (Integer) portal.get("yMin");
            int zMin = (Integer) portal.get("zMin");

            int xMax = (Integer) portal.get("xMax");
            int yMax = (Integer) portal.get("yMax");
            int zMax = (Integer) portal.get("zMax");

            String type = (String) portal.get("type");
            String name = (String) portal.get("name");
            String serverId = (String) portal.get("serverId");

            World worldObj = Bukkit.getWorld(world);

            Region3i.Rectangle x = Region3i.Rectangle.of(xMin, xMax);
            Region3i.Rectangle y = Region3i.Rectangle.of(yMin, yMax);
            Region3i.Rectangle z = Region3i.Rectangle.of(zMin, zMax);

            Region3i region3i = new Region3i(worldObj, x, y, z);


            if (type.equalsIgnoreCase("server")) {
                ret.put(name, new ConnectionPortal(api, new Server(serverId, "", "", 0, 0), name, region3i));
            } else if (type.equalsIgnoreCase("collection")) {
                ret.put(name, new ConnectionPortal(api, new ServerCollection(serverId, Collections.emptyMap()), name, region3i));
            }
        }
        return ret;
    }

    @Override
    protected Map<String, Object> serialize(ConnectionPortal portal) {
        Map<String, Object> ret = new LinkedHashMap<>();
        ret.put("world", portal.getRegion().getWorld().getName());
        ret.put("xMin", portal.getRegion().getX().getMin());
        ret.put("yMin", portal.getRegion().getY().getMin());
        ret.put("zMin", portal.getRegion().getZ().getMin());

        ret.put("xMax", portal.getRegion().getX().getMax());
        ret.put("yMax", portal.getRegion().getY().getMax());
        ret.put("zMax", portal.getRegion().getZ().getMax());

        ret.put("type", portal.getConnectable() instanceof Server ? "server" : "collection");
        ret.put("name", portal.getName());
        ret.put("serverId", portal.getConnectable().getName());
        return ret;
    }
}
