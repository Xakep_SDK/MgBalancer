package dk.xakeps.mgbalancer.spigot.portal;

import dk.xakeps.mgbalancer.spigot.storage.PortalStorage;
import org.bukkit.Location;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockFromToEvent;
import org.bukkit.event.player.PlayerMoveEvent;

import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class PortalManager implements Listener {
    private final PortalStorage storage;
    private final Map<String, ConnectionPortal> portals;

    public PortalManager(PortalStorage storage) {
        this.storage = storage;
        this.portals = storage.getContent();
    }

    public void addPortal(ConnectionPortal portal) {
        portals.put(portal.getName(), portal);
        storage.save();
    }

    public ConnectionPortal removePortal(String name) {
        ConnectionPortal portal = portals.remove(name);
        if (portal != null) {
            storage.save();
        }
        return portal;
    }

    public Set<String> getPortalNames() {
        return new HashSet<>(portals.keySet());
    }

    @EventHandler
    public void onPlayerMove(PlayerMoveEvent event) {
        for (ConnectionPortal portal : portals.values()) {
            if(portal.getRegion().contains(event.getFrom())) {
                portal.connect(event.getPlayer());
                return;
            }
        }
    }

    @EventHandler
    public void onFlow(BlockFromToEvent event) {
        Location location = event.getBlock().getLocation();
        for (ConnectionPortal portal : portals.values()) {
            if (portal.getRegion().contains(location)) {
                event.setCancelled(true);
                return;
            }
        }
    }
}
