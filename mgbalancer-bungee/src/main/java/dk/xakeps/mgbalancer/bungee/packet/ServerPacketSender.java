package dk.xakeps.mgbalancer.bungee.packet;

import dk.xakeps.mgbalancer.shared.Constants;
import dk.xakeps.mgbalancer.shared.packet.PacketSender;
import net.md_5.bungee.api.config.ServerInfo;
import net.md_5.bungee.api.connection.Server;

import java.util.Objects;

public class ServerPacketSender implements PacketSender {
    private final Server server;
    private final ServerInfo serverInfo;

    public ServerPacketSender(Server server) {
        this.server = Objects.requireNonNull(server);
        this.serverInfo = null;
    }

    public ServerPacketSender(ServerInfo serverInfo) {
        this.server = null;
        this.serverInfo = Objects.requireNonNull(serverInfo);
    }

    @Override
    public void send(byte[] data) {
        if (server != null) {
            server.sendData(Constants.CHANNEL_NAME, data);
        } else {
            serverInfo.sendData(Constants.CHANNEL_NAME, data);
        }
    }
}
