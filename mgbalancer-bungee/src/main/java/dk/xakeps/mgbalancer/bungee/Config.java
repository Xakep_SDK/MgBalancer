package dk.xakeps.mgbalancer.bungee;

import dk.xakeps.mgbalancer.shared.Server;
import dk.xakeps.mgbalancer.shared.ServerCollection;
import dk.xakeps.mgbalancer.shared.packet.data.SignTemplate;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.scheduler.ScheduledTask;
import net.md_5.bungee.config.Configuration;
import net.md_5.bungee.config.ConfigurationProvider;
import net.md_5.bungee.config.YamlConfiguration;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.FileAlreadyExistsException;
import java.nio.file.Files;
import java.util.*;
import java.util.concurrent.Phaser;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

public class Config {

    private Map<String, String> serverMaps;
    private SignTemplate template;
    private List<String> bannedMotds;
    private ServersHolder serversHolder;
    private ScheduledTask task;

    private Config(Configuration config, BungeePlugin plugin) {
        this.serverMaps = new HashMap<>();
        Configuration maps = config.getSection("maps");
        for (String serverName : maps.getKeys()) {
            String map = maps.getString(serverName);
            serverMaps.put(serverName, map);
        }

        List<String> serverTemplate = trimAndFill(config.getStringList("serverTemplate"));
        List<String> offlineServerTemplate = trimAndFill(config.getStringList("offlineServerTemplate"));
        List<String> collectionTemplate = trimAndFill(config.getStringList("collectionTemplate"));
        List<String> offlineCollectionTemplate = trimAndFill(config.getStringList("offlineCollectionTemplate"));

        this.template = new SignTemplate(serverTemplate, offlineServerTemplate, collectionTemplate, offlineCollectionTemplate);
        this.bannedMotds = config.getStringList("bannedMotds");
        this.bannedMotds.replaceAll(s -> s.toLowerCase(Locale.ROOT).trim());
        this.serversHolder = new ServersHolder(plugin.getProxy(), serverMaps);

        this.task = plugin.getProxy().getScheduler().schedule(plugin, serversHolder, 0, 1, TimeUnit.SECONDS);
    }

    public Map<String, ServerCollection> getServers() {
        return new HashMap<>(serversHolder.servers);
    }

    public SignTemplate getTemplate() {
        return template;
    }

    public List<String> getBannedMotds() {
        return bannedMotds;
    }

    void reload(BungeePlugin bungeePlugin, String name) throws IOException {
        Config load = load(bungeePlugin, name);
        this.task.cancel();
        this.serverMaps = load.serverMaps;
        this.template = load.template;
        this.bannedMotds = load.bannedMotds;
        this.serversHolder = load.serversHolder;
        this.task = load.task;
    }

    static Config load(BungeePlugin bungeePlugin, String name) throws IOException {
        File file = new File(bungeePlugin.getDataFolder(), name);
        if(!file.getParentFile().exists() && !file.getParentFile().mkdirs()) {
            throw new FileNotFoundException("Can't create plugin directory!");
        }
        try(InputStream is = Config.class.getResourceAsStream("/config.yml")) {
            Files.copy(is, file.toPath());
        } catch (FileAlreadyExistsException ignored) {
        } catch (IOException e) {
            throw new IOException("Can't save config!", e);
        }
        Configuration config = ConfigurationProvider
                .getProvider(YamlConfiguration.class)
                .load(file);
        return new Config(config, bungeePlugin);
    }

    private static List<String> trimAndFill(List<String> list) {
        List<String> l = Arrays.asList("", "", "", "");
        for (int i = 0; i < 4; i++) {
            if (list.size() > i) {
                l.set(i, list.get(i));
            }
        }
        return l;
    }

    private static final class ServersHolder implements Runnable {
        private ProxyServer proxyServer;
        private Map<String, String> serverMaps;
        private Map<String, ServerCollection> servers;

        private ServersHolder(ProxyServer proxyServer, Map<String, String> serverMaps) {
            this.proxyServer = proxyServer;
            this.serverMaps = serverMaps;
            this.servers = new HashMap<>();
        }

        @Override
        public void run() {
            Map<String, MutableServerCollection> map = new HashMap<>();
            Phaser phaser = new Phaser(1);
            proxyServer.getServers().forEach((name, serverInfo) -> {
                String collectionName = Server.getCollectionName(name);
                MutableServerCollection n = map.computeIfAbsent(collectionName, MutableServerCollection::new);
                phaser.register();
                serverInfo.ping((serverPing, throwable) -> {
                    if(throwable != null) {
                        phaser.arrive();
                        return;
                    }
                    n.getServers().put(name,
                            new Server(name,
                                    serverMaps.getOrDefault(name, ""),
                                    serverPing.getDescriptionComponent().toLegacyText(),
                                    serverPing.getPlayers().getOnline(),
                                    serverPing.getPlayers().getMax()));
                    phaser.arrive();
                });
            });
            phaser.arriveAndAwaitAdvance();
            servers = map.entrySet().stream()
                    .collect(Collectors.toMap(Map.Entry::getKey, e -> e.getValue().asImmutable()));
        }
    }
}
