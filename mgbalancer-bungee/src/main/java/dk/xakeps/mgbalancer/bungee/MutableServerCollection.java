package dk.xakeps.mgbalancer.bungee;

import dk.xakeps.mgbalancer.shared.Server;
import dk.xakeps.mgbalancer.shared.ServerCollection;

import java.util.HashMap;
import java.util.Map;

public class MutableServerCollection {
    private final String id;
    private final Map<String, Server> servers;

    public MutableServerCollection(String id) {
        this.id = id;
        this.servers = new HashMap<>();
    }

    public String getId() {
        return id;
    }

    public Map<String, Server> getServers() {
        return servers;
    }

    public ServerCollection asImmutable() {
        return new ServerCollection(id, servers);
    }
}
