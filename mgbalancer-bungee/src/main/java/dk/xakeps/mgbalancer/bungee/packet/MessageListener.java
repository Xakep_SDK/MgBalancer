package dk.xakeps.mgbalancer.bungee.packet;

import dk.xakeps.mgbalancer.bungee.BungeeBalancer;
import dk.xakeps.mgbalancer.bungee.BungeePlugin;
import dk.xakeps.mgbalancer.shared.Constants;
import dk.xakeps.mgbalancer.shared.packet.Packet;
import dk.xakeps.mgbalancer.shared.packet.PacketProcessor;
import net.md_5.bungee.api.connection.Server;
import net.md_5.bungee.api.event.PluginMessageEvent;
import net.md_5.bungee.api.plugin.Listener;
import net.md_5.bungee.event.EventHandler;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;

public class MessageListener implements Listener {
    private final BungeePlugin bungeePlugin;
    private final PacketProcessor processor;
    private final BungeeBalancer balancer;

    public MessageListener(BungeePlugin bungeePlugin, PacketProcessor processor, BungeeBalancer balancer) {
        this.bungeePlugin = bungeePlugin;
        this.processor = processor;
        this.balancer = balancer;
    }

    @EventHandler
    public void onPluginMessage(PluginMessageEvent event) {
        bungeePlugin.getProxy().getScheduler().runAsync(bungeePlugin, () -> {
            if (event.getTag().equalsIgnoreCase(Constants.CHANNEL_NAME)) {
                if(event.getSender() instanceof Server) {
                    try (ObjectInputStream ois = new ObjectInputStream(new ByteArrayInputStream(event.getData()))) {
                        Packet packet = (Packet) ois.readObject();
                        packet.process(processor, balancer.getSender((Server) event.getSender()));
                    } catch (IOException | ClassNotFoundException e) {
                        e.printStackTrace();
                    }
                }
            }
        });
    }
}
