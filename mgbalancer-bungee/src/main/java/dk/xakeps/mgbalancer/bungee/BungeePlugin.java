package dk.xakeps.mgbalancer.bungee;

import dk.xakeps.mgbalancer.bungee.packet.BungeePacketProcessor;
import dk.xakeps.mgbalancer.bungee.packet.MessageListener;
import dk.xakeps.mgbalancer.shared.Constants;
import dk.xakeps.mgbalancer.shared.Server;
import dk.xakeps.mgbalancer.shared.ServerCollection;
import dk.xakeps.mgbalancer.shared.packet.Packet;
import dk.xakeps.mgbalancer.shared.packet.request.PacketSignTemplateUpdateRequest;
import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.config.ServerInfo;
import net.md_5.bungee.api.plugin.Command;
import net.md_5.bungee.api.plugin.Plugin;

import java.io.IOException;
import java.util.Map;
import java.util.Optional;

public class BungeePlugin extends Plugin {

    private Config config;
    private BungeeBalancer balancer;

    @Override
    public void onEnable() {
        try {
            this.config = Config.load(this, "config.yml");
        } catch (IOException e) {
            getLogger().severe("Failed while loading config!");
            throw new RuntimeException(e);
        }

        balancer = new BungeeBalancer(this, config);
        getProxy().registerChannel(Constants.CHANNEL_NAME);
        getProxy().getPluginManager().registerListener(this,
                new MessageListener(this, new BungeePacketProcessor(balancer), balancer));
        getProxy().getPluginManager().registerCommand(this, new ReloadConfigCommand());
        getProxy().getPluginManager().registerCommand(this, new ServerInfoCommand());
    }

    @Override
    public void onDisable() {
        getProxy().unregisterChannel(Constants.CHANNEL_NAME);
        getProxy().getPluginManager().unregisterListeners(this);
    }

    private class ReloadConfigCommand extends Command {
        private ReloadConfigCommand() {
            super("mgbr", "mgb.use");
        }

        @Override
        public void execute(CommandSender commandSender, String[] strings) {
            getProxy().getScheduler().runAsync(BungeePlugin.this, () -> {
                try {
                    config.reload(BungeePlugin.this,"config.yml");
                    Packet request = new PacketSignTemplateUpdateRequest(config.getTemplate());
                    for (ServerInfo server : getProxy().getServers().values()) {
                        balancer.getSender(server).send(request);
                    }
                } catch (IOException e) {
                    getLogger().severe("Failed while reloading config!");
                    throw new RuntimeException(e);
                }
            });
        }
    }

    private class ServerInfoCommand extends Command {
        private ServerInfoCommand() {
            super("mgbi", "mgb.use");
        }

        @Override
        public void execute(CommandSender commandSender, String[] args) {
            getProxy().getScheduler().runAsync(BungeePlugin.this, () -> {
                if (args.length != 2) {
                    commandSender.sendMessage(new TextComponent("[MGB] Команда выводит информацию о сервере или коллекции Пример использования: /mgbi <server|coll> <id>!"));
                    return;
                }

                String type = args[0];
                String id = args[1];
                if ("coll".equals(type)) {
                    ServerCollection serverCollection = config.getServers().get(id);
                    if (serverCollection == null) {
                        commandSender.sendMessage(new TextComponent("[MGB] Коллекция не найдена!"));
                        return;
                    }
                    commandSender.sendMessage(new TextComponent("[MGB] Информация о коллекции: " + replaceSectionSign(serverCollection.toString())));
                } else if ("server".equals(type)) {
                    String collectionName;
                    try {
                        collectionName = Server.getCollectionName(id);
                    } catch (IllegalArgumentException e) {
                        commandSender.sendMessage(new TextComponent("[MGB] Неверный ID сервера!"));
                        return;
                    }

                    Map<String, ServerCollection> colls = config.getServers();
                    ServerCollection serverCollection = colls.get(collectionName);
                    if (serverCollection == null) {
                        commandSender.sendMessage(new TextComponent("[MGB] Сервер не найден!"));
                        return;
                    }

                    Optional<Server> server = serverCollection.getServer(id);
                    if (server.isPresent()) {
                        commandSender.sendMessage();
                        commandSender.sendMessage(new TextComponent("[MGB] Информация о сервере: " + replaceSectionSign(server.get().toString())));
                    } else {
                        commandSender.sendMessage(new TextComponent("[MGB] Сервер не найден!"));
                    }
                } else {
                    commandSender.sendMessage(new TextComponent("[MGB] Неверный тип запроса! Доступные типы: server, coll"));
                }
            });
        }

        private String replaceSectionSign(String textToTranslate) {
            char[] b = textToTranslate.toCharArray();
            for (int i = 0; i < b.length - 1; i++) {
                if (b[i] == ChatColor.COLOR_CHAR && ChatColor.ALL_CODES.indexOf(b[i + 1]) > -1) {
                    b[i] = '&';
                    b[i + 1] = Character.toLowerCase(b[i + 1]);
                }
            }
            return new String(b);
        }
    }
}
