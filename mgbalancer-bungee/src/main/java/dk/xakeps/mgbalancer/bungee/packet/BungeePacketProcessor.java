package dk.xakeps.mgbalancer.bungee.packet;

import dk.xakeps.mgbalancer.bungee.BungeeBalancer;
import dk.xakeps.mgbalancer.shared.BalancerPlayer;
import dk.xakeps.mgbalancer.shared.Connectable;
import dk.xakeps.mgbalancer.shared.Server;
import dk.xakeps.mgbalancer.shared.ServerCollection;
import dk.xakeps.mgbalancer.shared.packet.Packet;
import dk.xakeps.mgbalancer.shared.packet.PacketProcessor;
import dk.xakeps.mgbalancer.shared.packet.PacketSender;
import dk.xakeps.mgbalancer.shared.packet.request.PacketConnectPlayerToCollectionRequest;
import dk.xakeps.mgbalancer.shared.packet.request.PacketConnectPlayerToServerRequest;
import dk.xakeps.mgbalancer.shared.packet.request.PacketServerListRequest;
import dk.xakeps.mgbalancer.shared.packet.request.PacketSignTemplateRequest;
import dk.xakeps.mgbalancer.shared.packet.response.PacketConnectPlayerResponse;
import dk.xakeps.mgbalancer.shared.packet.response.PacketServerListResponse;
import dk.xakeps.mgbalancer.shared.packet.response.PacketSignTemplateResponse;

import java.io.IOException;

public class BungeePacketProcessor implements PacketProcessor {
    private final BungeeBalancer balancer;

    public BungeePacketProcessor(BungeeBalancer balancer) {
        this.balancer = balancer;
    }

    @Override
    public void process(PacketServerListRequest packet, PacketSender sender) {
        balancer.getServers()
                .thenApply(servers -> new PacketServerListResponse(packet.getRequestId(), servers))
                .thenAccept(response -> sendPacket(response, sender));
    }

    @Override
    public void process(PacketConnectPlayerToServerRequest packet, PacketSender sender) {
        balancer.getPlayerFromId(packet.getPlayerId())
                .thenCombine(balancer.getServers(), (player, colls) -> {
                    ServerCollection coll = colls.get(Server.getCollectionName(packet.getServer()));
                    if(coll == null) return new ConnectableHolder(player, null);
                    return new ConnectableHolder(player, coll.getServer(packet.getServer()).orElse(null));
                })
                .thenCompose(holder -> balancer.connect(holder.player, holder.connectable))
                .thenApply(r -> new PacketConnectPlayerResponse(packet.getRequestId(), r))
                .thenAccept(response -> sendPacket(response, sender));
    }

    @Override
    public void process(PacketSignTemplateRequest packet, PacketSender sender) {
        balancer.getTemplate().thenApply(signTemplate -> new PacketSignTemplateResponse(packet.getRequestId(), signTemplate))
                .thenAccept(response -> sendPacket(response, sender));
    }

    @Override
    public void process(PacketConnectPlayerToCollectionRequest packet, PacketSender sender) {
        balancer.getPlayerFromId(packet.getPlayerId())
                .thenCombine(balancer.getServers(), (player, colls) -> new ConnectableHolder(player, colls.get(packet.getCollectionName())))
                .thenCompose(holder -> balancer.connect(holder.player, holder.connectable))
                .thenApply(connectResponse -> new PacketConnectPlayerResponse(packet.getRequestId(), connectResponse))
                .thenAccept(response -> sendPacket(response, sender));
    }

    private static void sendPacket(Packet packet, PacketSender sender) {
        try {
            sender.send(packet);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static final class ConnectableHolder {
        private final BalancerPlayer player;
        private final Connectable connectable;

        private ConnectableHolder(BalancerPlayer player, Connectable connectable) {
            this.player = player;
            this.connectable = connectable;
        }
    }
}
