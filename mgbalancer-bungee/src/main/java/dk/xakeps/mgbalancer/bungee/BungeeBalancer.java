package dk.xakeps.mgbalancer.bungee;

import dk.xakeps.mgbalancer.bungee.packet.ServerPacketSender;
import dk.xakeps.mgbalancer.shared.Balancer;
import dk.xakeps.mgbalancer.shared.BalancerPlayer;
import dk.xakeps.mgbalancer.shared.Server;
import dk.xakeps.mgbalancer.shared.ServerCollection;
import dk.xakeps.mgbalancer.shared.packet.PacketSender;
import dk.xakeps.mgbalancer.shared.packet.data.ConnectResponse;
import dk.xakeps.mgbalancer.shared.packet.data.SignTemplate;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.config.ServerInfo;
import net.md_5.bungee.api.connection.ProxiedPlayer;

import java.util.*;
import java.util.concurrent.CompletableFuture;

public class BungeeBalancer implements Balancer {
    private static final Comparator<InfoHolder> COMPARATOR;
    private static BungeeBalancer instance;
    private final BungeePlugin plugin;
    private final ProxyServer proxyServer;
    private final Config config;

    static {
        Comparator<InfoHolder> c = Comparator.comparingInt(o -> o.getServer().getOnline());
        COMPARATOR = c.reversed();
    }

    BungeeBalancer(BungeePlugin plugin, Config config) {
        this.plugin = plugin;
        if(instance != null) throw new IllegalStateException("Already initialized!");
        this.proxyServer = plugin.getProxy();
        this.config = config;
        instance = this;
    }

    @Override
    public CompletableFuture<ConnectResponse> connect(BalancerPlayer player, Server server) {
        if(server == null) return CompletableFuture.completedFuture(new ConnectResponse(false, false));
        ProxiedPlayer pp = proxyServer.getPlayer(player.getUniqueId());
        ServerInfo serverInfo = proxyServer.getServers().get(server.getName());
        CompletableFuture<ConnectResponse> future = new CompletableFuture<>();
        if(serverInfo != null && !config.getBannedMotds().contains(server.getMotd().toLowerCase(Locale.ROOT).trim())) {
            pp.connect(serverInfo, (connected, exception) -> {
                future.complete(new ConnectResponse(connected, exception != null));
            });
        } else {
            future.complete(new ConnectResponse(false, false));
        }
        return future;
    }

    @Override
    public CompletableFuture<ConnectResponse> connect(BalancerPlayer player, ServerCollection collection) {
        if(collection == null) return CompletableFuture.completedFuture(new ConnectResponse(false, false));
        // Тут должен быть TreeSet с правильным компаратором, но у заказчика bungeecord от Leymooo (slavik123123123)
        // и в его банджи что-то сломанно, поэтому вызов API ServerInfo.getPlayers() всегда возвращает пустую коллекцию.
        // 20.12.2017
        Set<InfoHolder> servers = new TreeSet<>(COMPARATOR);
        for (Server server : collection.getServers()) {
            ServerInfo info = proxyServer.getServers().get(server.getName());
            if(info != null && !config.getBannedMotds().contains(server.getMotd().toLowerCase(Locale.ROOT).trim())) {
                servers.add(new InfoHolder(server, info));
            }
        }
        CompletableFuture<ConnectResponse> future = new CompletableFuture<>();
        ProxiedPlayer p = proxyServer.getPlayer(player.getUniqueId());

        Iterator<InfoHolder> iterator = servers.iterator();
        proxyServer.getScheduler().runAsync(plugin, () -> connect(p, iterator, future));
        return future;
    }

    @Override
    public CompletableFuture<SignTemplate> getTemplate() {
        return CompletableFuture.completedFuture(config.getTemplate());
    }

    @Override
    public CompletableFuture<Map<String, ServerCollection>> getServers() {
        return CompletableFuture.completedFuture(config.getServers());
    }

    @Override
    public PacketSender getSender() {
        throw new UnsupportedOperationException();
    }

    @Override
    public PacketSender getSender(Server server) {
        return getSender(proxyServer.getServerInfo(server.getName()));
    }

    @Override
    public CompletableFuture<BalancerPlayer> getPlayerFromId(UUID playerId) {
        return CompletableFuture.completedFuture(() -> proxyServer.getPlayer(playerId).getUniqueId());
    }

    public PacketSender getSender(ServerInfo info) {
        return new ServerPacketSender(info);
    }

    public PacketSender getSender(net.md_5.bungee.api.connection.Server server) {
        return new ServerPacketSender(server);
    }

    public static BungeeBalancer getInstance() {
        return Objects.requireNonNull(instance, "Not initialized!");
    }

    private static void connect(ProxiedPlayer player, Iterator<InfoHolder> servers, CompletableFuture<ConnectResponse> future) {
        if (servers.hasNext()) {
            ServerInfo info = servers.next().getInfo();
            if(player.getServer().getInfo().equals(info)) {
                connect(player, servers, future);
            } else {
                player.connect(info, (connected, throwable) -> {
                    if(throwable != null) throwable.printStackTrace();
                    if(connected) {
                        future.complete(new ConnectResponse(true, throwable != null));
                    } else {
                        connect(player, servers, future);
                    }
                });
            }
        } else {
            future.complete(new ConnectResponse(false, false));
        }
    }

    private static final class InfoHolder {
        private final Server server;
        private final ServerInfo info;

        private InfoHolder(Server server, ServerInfo info) {
            this.server = server;
            this.info = info;
        }

        private Server getServer() {
            return server;
        }

        private ServerInfo getInfo() {
            return info;
        }
    }
}
