package dk.xakeps.mgbalancer.shared.packet;

import dk.xakeps.mgbalancer.shared.packet.request.*;
import dk.xakeps.mgbalancer.shared.packet.response.PacketConnectPlayerResponse;
import dk.xakeps.mgbalancer.shared.packet.response.PacketServerListResponse;
import dk.xakeps.mgbalancer.shared.packet.response.PacketSignTemplateResponse;

public interface PacketProcessor {
    default void process(PacketServerListRequest packet, PacketSender sender) {}
    default void process(PacketServerListResponse packet, PacketSender sender) {}
    default void process(PacketConnectPlayerToServerRequest packet, PacketSender sender) {}
    default void process(PacketConnectPlayerResponse packet, PacketSender sender) {}
    default void process(PacketConnectPlayerToCollectionRequest packet, PacketSender sender) {}
    default void process(PacketSignTemplateRequest packet, PacketSender sender) {}
    default void process(PacketSignTemplateResponse packet, PacketSender sender) {}
    default void process(PacketSignTemplateUpdateRequest packet, PacketSender sender) {}
}
