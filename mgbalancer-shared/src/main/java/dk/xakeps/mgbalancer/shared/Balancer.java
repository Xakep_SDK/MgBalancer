package dk.xakeps.mgbalancer.shared;

import dk.xakeps.mgbalancer.shared.packet.PacketSender;
import dk.xakeps.mgbalancer.shared.packet.data.ConnectResponse;
import dk.xakeps.mgbalancer.shared.packet.data.SignTemplate;

import java.util.Map;
import java.util.UUID;
import java.util.concurrent.CompletableFuture;

public interface Balancer {
    CompletableFuture<ConnectResponse> connect(BalancerPlayer player, Server server);

    CompletableFuture<ConnectResponse> connect(BalancerPlayer player, ServerCollection collection);

    default CompletableFuture<ConnectResponse> connect(BalancerPlayer player, Connectable connectable) {
        if (connectable instanceof Server) {
            return connect(player, (Server) connectable);
        } else if (connectable instanceof ServerCollection) {
            return connect(player, (ServerCollection) connectable);
        } else {
            return CompletableFuture.completedFuture(new ConnectResponse(false, false));
        }
    }

    CompletableFuture<SignTemplate> getTemplate();

    CompletableFuture<Map<String, ServerCollection>> getServers();

    PacketSender getSender();

    PacketSender getSender(Server server);

    CompletableFuture<BalancerPlayer> getPlayerFromId(UUID playerID);
}
