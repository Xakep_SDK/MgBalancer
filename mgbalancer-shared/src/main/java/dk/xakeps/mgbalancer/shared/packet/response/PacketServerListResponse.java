package dk.xakeps.mgbalancer.shared.packet.response;

import dk.xakeps.mgbalancer.shared.ServerCollection;
import dk.xakeps.mgbalancer.shared.packet.Packet;
import dk.xakeps.mgbalancer.shared.packet.PacketProcessor;
import dk.xakeps.mgbalancer.shared.packet.PacketSender;

import java.io.Serializable;
import java.util.Map;

public class PacketServerListResponse extends Packet implements Serializable {
    private static final long serialVersionUID = -7255814589460792123L;

    private final Map<String, ServerCollection> servers;

    public PacketServerListResponse(int requestId, Map<String, ServerCollection> servers) {
        super(requestId);
        this.servers = servers;
    }

    public Map<String, ServerCollection> getServers() {
        return servers;
    }

    @Override
    public void process(PacketProcessor processor, PacketSender sender) {
        processor.process(this, sender);
    }
}
