package dk.xakeps.mgbalancer.shared;

import com.google.common.cache.Cache;
import com.google.common.cache.CacheBuilder;
import com.google.common.cache.RemovalListener;
import dk.xakeps.mgbalancer.shared.packet.Packet;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import java.util.concurrent.atomic.AtomicInteger;

public class ResponseQueue {
    private final AtomicInteger idQueue;
    private final Cache<Integer, CompletableFuture<?>> requestCache;

    public ResponseQueue(long timeout) {
        this.idQueue = new AtomicInteger();
        this.requestCache = CacheBuilder.newBuilder()
                .expireAfterWrite(timeout, TimeUnit.MILLISECONDS)
                .removalListener((RemovalListener<Integer, CompletableFuture<?>>) n ->
                        n.getValue().completeExceptionally(new TimeoutException("Packet took too long time to response")))
                .build();
        Executors.newSingleThreadScheduledExecutor()
                .scheduleAtFixedRate(requestCache::cleanUp, 0, 5, TimeUnit.SECONDS);
    }

    public int registerPacketResponse(@Nonnull Packet packet, @Nonnull CompletableFuture<?> future) {
        int id = idQueue.incrementAndGet();
        packet.setRequestId(id);
        requestCache.put(id, future);
        return id;
    }

    @SuppressWarnings("unchecked")
    @Nullable
    public <T> CompletableFuture<T> getPacketResponse(int id) {
        return (CompletableFuture<T>) requestCache.getIfPresent(id);
    }
}
