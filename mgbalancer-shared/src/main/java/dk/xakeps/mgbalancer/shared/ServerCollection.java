package dk.xakeps.mgbalancer.shared;

import java.io.Serializable;
import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;

public final class ServerCollection implements Connectable, Serializable {
    private static final long serialVersionUID = 4123491189464552915L;

    private final String name;
    private final Map<String, Server> servers;

    public ServerCollection(String name, Map<String, Server> servers) {
        this.name = name;
        this.servers = servers;
    }

    @Override
    public String getName() {
        return name;
    }

    public Optional<Server> getServer(String name) {
        return Optional.ofNullable(servers.get(name));
    }

    public Set<Server> getServers() {
        return Collections.unmodifiableSet(new HashSet<>(servers.values()));
    }

    public int getOnline() {
        return servers.values().stream().mapToInt(Server::getOnline).sum();
    }

    public int getMaxOnline() {
        return servers.values().stream().mapToInt(Server::getMaxOnline).sum();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ServerCollection that = (ServerCollection) o;
        return Objects.equals(name, that.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name);
    }

    @Override
    public String toString() {
        return "ServerCollection{" +
                "name='" + name + '\'' +
                ", servers=" + servers +
                '}';
    }
}
