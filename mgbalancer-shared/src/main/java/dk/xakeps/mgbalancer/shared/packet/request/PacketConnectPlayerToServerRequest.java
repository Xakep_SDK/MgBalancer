package dk.xakeps.mgbalancer.shared.packet.request;

import dk.xakeps.mgbalancer.shared.packet.Packet;
import dk.xakeps.mgbalancer.shared.packet.PacketProcessor;
import dk.xakeps.mgbalancer.shared.packet.PacketSender;

import java.io.Serializable;
import java.util.UUID;

public class PacketConnectPlayerToServerRequest extends Packet implements Serializable {
    private static final long serialVersionUID = -2734527593714937457L;

    private final UUID playerId;
    private final String server;

    public PacketConnectPlayerToServerRequest(UUID playerId, String server) {
        this.playerId = playerId;
        this.server = server;
    }

    public UUID getPlayerId() {
        return playerId;
    }

    public String getServer() {
        return server;
    }

    @Override
    public void process(PacketProcessor processor, PacketSender sender) {
        processor.process(this, sender);
    }
}
