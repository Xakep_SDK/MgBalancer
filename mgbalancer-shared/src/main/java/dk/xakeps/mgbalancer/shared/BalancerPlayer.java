package dk.xakeps.mgbalancer.shared;

import java.util.UUID;

public interface BalancerPlayer {
    UUID getUniqueId();
}
