package dk.xakeps.mgbalancer.shared.packet;

import java.io.Serializable;

public abstract class Packet implements Serializable {
    private static final long serialVersionUID = -2831103545961093719L;

    private int requestId;

    public Packet() {
    }

    public Packet(int requestId) {
        this.requestId = requestId;
    }

    public int getRequestId() {
        return requestId;
    }

    public void setRequestId(int requestId) {
        this.requestId = requestId;
    }

    public abstract void process(PacketProcessor processor, PacketSender sender);
}
