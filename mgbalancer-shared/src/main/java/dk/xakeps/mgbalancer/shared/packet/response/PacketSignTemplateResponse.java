package dk.xakeps.mgbalancer.shared.packet.response;

import dk.xakeps.mgbalancer.shared.packet.data.SignTemplate;
import dk.xakeps.mgbalancer.shared.packet.Packet;
import dk.xakeps.mgbalancer.shared.packet.PacketProcessor;
import dk.xakeps.mgbalancer.shared.packet.PacketSender;

import java.io.Serializable;

public class PacketSignTemplateResponse extends Packet implements Serializable {
    private static final long serialVersionUID = -3291283091016777405L;

    private final SignTemplate template;

    public PacketSignTemplateResponse(int requestId, SignTemplate template) {
        super(requestId);
        this.template = template;
    }

    public SignTemplate getTemplate() {
        return template;
    }

    @Override
    public void process(PacketProcessor processor, PacketSender sender) {
        processor.process(this, sender);
    }
}
