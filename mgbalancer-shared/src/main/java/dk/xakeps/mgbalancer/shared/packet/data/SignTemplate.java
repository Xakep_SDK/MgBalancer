package dk.xakeps.mgbalancer.shared.packet.data;

import java.io.Serializable;
import java.util.List;
import java.util.Objects;

public class SignTemplate implements Serializable {
    private static final long serialVersionUID = -2063433159551476291L;

    private final List<String> serverTemplate;
    private final List<String> offlineServerTemplate;

    private final List<String> collectionTemplate;
    private final List<String> offlineCollectionTemplate;

    public SignTemplate(List<String> serverTemplate,
                        List<String> offlineServerTemplate,
                        List<String> collectionTemplate,
                        List<String> offlineCollectionTemplate) {
        this.serverTemplate = serverTemplate;
        this.offlineServerTemplate = offlineServerTemplate;
        this.collectionTemplate = collectionTemplate;
        this.offlineCollectionTemplate = offlineCollectionTemplate;
    }

    public List<String> getServerTemplate() {
        return serverTemplate;
    }

    public List<String> getOfflineServerTemplate() {
        return offlineServerTemplate;
    }

    public List<String> getCollectionTemplate() {
        return collectionTemplate;
    }

    public List<String> getOfflineCollectionTemplate() {
        return offlineCollectionTemplate;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        SignTemplate that = (SignTemplate) o;
        return Objects.equals(serverTemplate, that.serverTemplate) &&
                Objects.equals(offlineServerTemplate, that.offlineServerTemplate) &&
                Objects.equals(collectionTemplate, that.collectionTemplate) &&
                Objects.equals(offlineCollectionTemplate, that.offlineCollectionTemplate);
    }

    @Override
    public int hashCode() {

        return Objects.hash(serverTemplate, offlineServerTemplate, collectionTemplate, offlineCollectionTemplate);
    }

    @Override
    public String toString() {
        return "SignTemplate{" +
                "serverTemplate=" + serverTemplate +
                ", offlineServerTemplate=" + offlineServerTemplate +
                ", collectionTemplate=" + collectionTemplate +
                ", offlineCollectionTemplate=" + offlineCollectionTemplate +
                '}';
    }
}
