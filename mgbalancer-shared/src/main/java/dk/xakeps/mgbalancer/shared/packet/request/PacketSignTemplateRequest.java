package dk.xakeps.mgbalancer.shared.packet.request;

import dk.xakeps.mgbalancer.shared.packet.Packet;
import dk.xakeps.mgbalancer.shared.packet.PacketProcessor;
import dk.xakeps.mgbalancer.shared.packet.PacketSender;

import java.io.Serializable;

public class PacketSignTemplateRequest extends Packet implements Serializable {

    private static final long serialVersionUID = -3291283091016777405L;

    @Override
    public void process(PacketProcessor processor, PacketSender sender) {
        processor.process(this, sender);
    }
}
