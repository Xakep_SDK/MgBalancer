package dk.xakeps.mgbalancer.shared;

import java.io.Serializable;
import java.util.Objects;

public final class Server implements Connectable, Serializable {
    private static final long serialVersionUID = 2687390476586907308L;

    private final String name;
    private final String map;
    private final String motd;
    private final int online;
    private final int maxOnline;

    public Server(String name, String map, String motd, int online, int maxOnline) {
        this.name = name;
        this.map = map;
        this.motd = motd;
        this.online = online;
        this.maxOnline = maxOnline;
    }

    @Override
    public String getName() {
        return name;
    }

    public int getOnline() {
        return online;
    }

    public String getMap() {
        return map;
    }

    public String getMotd() {
        return motd;
    }

    public int getMaxOnline() {
        return maxOnline;
    }

    public String getCollectionName() {
        return getCollectionName(name);
    }

    public static String getCollectionName(String name) {
        try {
            return name.substring(0, name.indexOf('-'));
        } catch (IndexOutOfBoundsException e) {
            throw new IllegalArgumentException("Not a server ID! String=" + name);
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Server server = (Server) o;
        return Objects.equals(name, server.name);
    }

    @Override
    public int hashCode() {

        return Objects.hash(name);
    }

    @Override
    public String toString() {
        return "Server{" +
                "name='" + name + '\'' +
                ", map='" + map + '\'' +
                ", motd='" + motd + '\'' +
                ", online=" + online +
                ", maxOnline=" + maxOnline +
                '}';
    }
}
