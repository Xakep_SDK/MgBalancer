package dk.xakeps.mgbalancer.shared.packet.data;

import java.io.Serializable;

public class ConnectResponse implements Serializable {
    private static final long serialVersionUID = 6653972643107757365L;

    private final boolean connected;
    private final boolean error;

    public ConnectResponse(boolean connected, boolean error) {
        this.connected = connected;
        this.error = error;
    }

    public boolean isConnected() {
        return connected;
    }

    public boolean hasError() {
        return error;
    }
}
