package dk.xakeps.mgbalancer.shared;

import java.io.Serializable;

public interface Connectable extends Serializable {
    String getName();
}
