package dk.xakeps.mgbalancer.shared.packet.request;

import dk.xakeps.mgbalancer.shared.packet.data.SignTemplate;
import dk.xakeps.mgbalancer.shared.packet.Packet;
import dk.xakeps.mgbalancer.shared.packet.PacketProcessor;
import dk.xakeps.mgbalancer.shared.packet.PacketSender;

import java.io.Serializable;

public class PacketSignTemplateUpdateRequest extends Packet implements Serializable {
    private static final long serialVersionUID = 1134187760789816699L;

    private final SignTemplate template;

    public PacketSignTemplateUpdateRequest(SignTemplate template) {
        this.template = template;
    }

    public SignTemplate getTemplate() {
        return template;
    }

    @Override
    public void process(PacketProcessor processor, PacketSender sender) {
        processor.process(this, sender);
    }
}
