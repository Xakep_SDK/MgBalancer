package dk.xakeps.mgbalancer.shared.packet;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;

public interface PacketSender {
    void send(byte[] data);

    default void send(Packet packet) throws IOException {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        try(ObjectOutputStream os = new ObjectOutputStream(baos)) {
            os.writeObject(packet);
            send(baos.toByteArray());
        }
    }
}
