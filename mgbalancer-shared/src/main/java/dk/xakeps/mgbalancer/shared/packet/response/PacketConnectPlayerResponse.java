package dk.xakeps.mgbalancer.shared.packet.response;

import dk.xakeps.mgbalancer.shared.packet.Packet;
import dk.xakeps.mgbalancer.shared.packet.PacketProcessor;
import dk.xakeps.mgbalancer.shared.packet.PacketSender;
import dk.xakeps.mgbalancer.shared.packet.data.ConnectResponse;

import java.io.Serializable;

public class PacketConnectPlayerResponse extends Packet implements Serializable {
    private static final long serialVersionUID = 8734834626478389000L;

    private final ConnectResponse response;

    public PacketConnectPlayerResponse(int requestId, ConnectResponse response) {
        super(requestId);
        this.response = response;
    }

    public ConnectResponse getResponse() {
        return response;
    }

    @Override
    public void process(PacketProcessor processor, PacketSender sender) {
        processor.process(this, sender);
    }
}
