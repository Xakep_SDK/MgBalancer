package dk.xakeps.mgbalancer.shared.packet.request;

import dk.xakeps.mgbalancer.shared.packet.Packet;
import dk.xakeps.mgbalancer.shared.packet.PacketProcessor;
import dk.xakeps.mgbalancer.shared.packet.PacketSender;

import java.io.Serializable;
import java.util.UUID;

public class PacketConnectPlayerToCollectionRequest extends Packet implements Serializable {
    private static final long serialVersionUID = -900692090714305251L;
    private final UUID playerId;
    private final String collectionName;

    public PacketConnectPlayerToCollectionRequest(UUID playerId, String collectionName) {
        this.playerId = playerId;
        this.collectionName = collectionName;
    }

    public UUID getPlayerId() {
        return playerId;
    }

    public String getCollectionName() {
        return collectionName;
    }

    @Override
    public void process(PacketProcessor processor, PacketSender sender) {
        processor.process(this, sender);
    }
}
